'use strict'

let reekoh = require('reekoh')
let plugin = new reekoh.plugins.Service()
/* eslint-disable new-cap */
const rkhLogger = new reekoh.logger('ms-graph-service')

const get = require('lodash.get')

plugin.on('data', (data) => {
  let startTime = plugin.processStart()
  const request = require('request').defaults({ encoding: null })
  let oDataKey = plugin.config.odataMethodKey || 'odata'
  let method = get(data, oDataKey)
  let filter = get(data, 'filter')

  const headers = {
    'OData-MaxVersion': '4.0',
    'OData-Version': '4.0',
    'Cache-Control': 'no-cache',
    'Authorization': 'Bearer ' + data['access_token'],
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }

  let url = 'https://graph.microsoft.com/' + plugin.config.apiVersion + '/' + plugin.config.entitySchema
  if (filter) {
    if (method !== null && method !== '' && method !== undefined) {
      url += '?' + method
    }
  } else {
    url += method
  }

  let options = {
    url: url,
    method: 'GET',
    headers: headers,
    json: true
  }

  request(options, function (error, response, body) {
    if (error) {
      plugin.processDone(startTime)
      return plugin.logException(error)
    } else if (get(body, 'error')) {
      if (body.error.code === 'ErrorItemNotFound') {
        return plugin.pipe(data, { image: 'no image' })
          .then(() => {
            plugin.processDone(startTime)
            plugin.log({
              title: 'Microsoft Graph Service',
              result: 'no image'
            })
          })
          .catch(err => {
            plugin.processDone(startTime)
            plugin.logException(err)
          })
      }
      plugin.processDone(startTime)
      return plugin.logException(new Error(get(body, 'error')))
    } else if (response.statusCode !== 200) {
      if (!filter && response.statusCode === 404) {
        return plugin.pipe(data, { image: 'no image' })
          .then(() => {
            plugin.processDone(startTime)
            plugin.log({
              title: 'Microsoft Graph Service',
              result: 'no image'
            })
          })
          .catch(err => {
            plugin.processDone(startTime)
            plugin.logException(err)
          })
      }
    }

    if (response.headers['content-type'] === 'image/jpeg') {
      let image = Buffer.from(body).toString('base64')
      let result

      if (image) result = { image: image }
      else result = { image: 'no image' }

      plugin.pipe(data, result)
        .then(() => {
          plugin.processDone(startTime)
          plugin.log({
            title: 'Microsoft Graph Service',
            result: image
          })
        })
        .catch(err => {
          plugin.processDone(startTime)
          plugin.logException(err)
        })
    } else {
      plugin.pipe(data, body)
        .then(() => {
          plugin.processDone(startTime)
          plugin.log({
            title: 'Microsoft Graph Service',
            result: body
          })
        })
        .catch(err => {
          plugin.processDone(startTime)
          plugin.logException(err)
        })
    }
  })
})

plugin.once('ready', () => {
  rkhLogger.info('MS Graph Service has been initialized.')
  plugin.log('MS Graph Service has been initialized.')
  plugin.emit('init')
})

module.exports = plugin
